#!/usr/bin/env python
#
# Last Modification Date: 2021-01-09
# Author: Gilberto Noronha <gnoronha@burning-glass.com> 
#
# Please see the "README" file for basic installation and usage
# instructions.
#

from snowflake_helpers import make_logger

from skills_projections import Skill_growth_predictor

def main():

  log_file = "skills_projections.log"
  development_schema = "bgt_application_data.predictor"
  cache_directory = ".skill_growth_predictor_cache"
  bgt_schema = "bgt_derived_data.optimized_tables"
  stack_overflow_schema = "bgt_stack_overflow.specialized"
  connection_kwargs = {\
    "configuration_file" : "snowflake_config"\
  }

  logger = make_logger(log_file)

  skill_growth_predictor = Skill_growth_predictor(development_schema,
                                                  cache_directory,
                                                  bgt_schema,
                                                  stack_overflow_schema,
                                                  connection_kwargs)

  return skill_growth_predictor

if __name__ == "__main__":
  skill_growth_predictor = main()
