/*
Last Modification Date: 2021-01-19
Author: Gilberto Noronha <gnoronha@burning-glass.com>

Please see the "README" file for basic installation and usage
instructions.
alter session set WEEK_START=7;
*/




with date_dimension as (

select

replace(DATE::string,'-') as DATE_ID,

DATE as DATE_VALUE,

YEAR as DATE_YEAR,

substring(Quarter_name,2,1)::number as DATE_QUARTER,

MONTH as DATE_MONTH,

WEEK as DATE_WEEK,

concat(year::string,'0',substring(Quarter_name,2,1)) as YEAR_QUARTER,

substring(replace(DATE::string,'-'),1,6) as YEAR_MONTH,

IFF (WEEK<10, (concat(year(date_trunc(week, date))::string,'0',week::string)),(concat(year(date_trunc(week, date))::string,week::string)))  as YEAR_WEEK,

dayofweek(date) as DAY_OF_WEEK,

date_trunc(year, date) as YEAR_START,

last_day(date,'year') as YEAR_END,

date_trunc(quarter, date) as QUARTER_START,

last_day(date,'quarter') as QUARTER_END,

date_trunc(month, date) as MONTH_START,

last_day(date,'month') as MONTH_END,

date_trunc(week, date) as WEEK_START,

last_day(date,'week') as WEEK_END

from "BGT_DATA_34"."PUBLIC"."D_DATE"),
 general_table as (

select skill_name_en_us as variable_name,'skill' as variable_type,  skill_id, IFF(skill_name_en_us is not null, skill_name_en_us, IFF(get(skill_name,'en_SG')::string is not null, get(skill_name,'en_SG')::string,IFF(get(skill_name,'en_AU')::string is not null, get(skill_name,'en_AU')::string,null)))  as skill,
skill_cluster_name_en_us as skill_cluster, skill_cluster_family_name_en_us as skill_cluster_family
from BGT_DATA_34.PUBLIC.D_BGT_SKILL_CURRENT_PRODUCTION
where IFF(skill_name_en_us is not null, skill_name_en_us, IFF(get(skill_name,'en_SG')::string is not null, get(skill_name,'en_SG')::string,IFF(get(skill_name,'en_AU')::string is not null, get(skill_name,'en_AU')::string,null))) is not null
union all
select distinct(skill_cluster_name_en_us) as variable_name,'skill_cluster' as variable_type,  'NULL' as skill_id, 'NULL' as skill,
skill_cluster_name_en_us as skill_cluster, skill_cluster_family_name_en_us as skill_cluster_family
from BGT_DATA_34.PUBLIC.D_BGT_SKILL_CURRENT_PRODUCTION
where skill_cluster_name_en_us is not null
union all
select distinct(skill_cluster_family_name_en_us) as variable_name,'skill_cluster_family' as variable_type,  'NULL' as skill_id, 'NULL' as skill,
'NULL' as skill_cluster, skill_cluster_family_name_en_us as skill_cluster_family
from BGT_DATA_34.PUBLIC.D_BGT_SKILL_CURRENT_PRODUCTION
where skill_cluster_family_name_en_us is not null),
skill_variables_atlas as (
select 'US' as geography, a.* from general_table as a
union all
select 'GB' as geography, a.* from general_table as a
union all
select 'CA' as geography, a.* from general_table as a
union all
select 'AU' as geography, a.* from general_table as a
union all
select 'SG' as geography, a.* from general_table as a
union all
select 'NZ' as geography, a.* from general_table as a
union all
select 'all' as geography, a.* from general_table as a),
skill_variables_old as (
select a.*, row_number()

  over (order by geography desc) as variable_id

  from skill_variables_atlas as a),
loc_date as (
            SELECT v.vacancy_key as bgtjobid, loc.LOCATION_010000_KEY, loc.COUNTY_REGION_EN as county, loc.METROPOLITAN_AREA_NAME_EN as bgtmsaname, loc.CITY_NAME_EN as canoncity,
                        loc.STATE_PROVINCE_NAME_EN as statename, loc.COUNTRY_NAME_EN as countryname, loc.ALPHA_2_CODE as country_code, dt.date as job_date, ldt.date as job_expire_date
            FROM (SELECT vk.vacancy_key, LOCATION_010000_KEY[0] AS LOCATION_KEY, vk.FIRST_ACTIVE_DATE_KEY, vk.last_active_date_key
                  FROM BGT_DATA_34.PUBLIC.F_BGT_VACANCY vk
                  ---TABLE(FLATTEN(vk.LOCATION_010000_KEY)) geo
                  WHERE LOCATION_KEY IN (SELECT dloc.LOCATION_010000_KEY FROM BGT_DATA_34.PUBLIC.D_LOCATION_010000 dloc)
                  and first_active_date_key is not null
                  --order by first_active_date_key desc
                  ) v
            INNER JOIN BGT_DATA_34.PUBLIC.D_LOCATION_010000 loc ON v.LOCATION_KEY = loc.LOCATION_010000_KEY
            Inner Join BGT_DATA_34.PUBLIC.D_DATE dt ON v.FIRST_ACTIVE_DATE_KEY = dt.DATE_KEY
            Inner Join BGT_DATA_34.PUBLIC.D_DATE ldt ON v.last_active_date_key = ldt.DATE_KEY
            where year(job_date)>=2000),
  base_table as
(
select
a.variable_id,
b.geography,
b.variable_type,
b.variable_name,
b.job_posting_year_month as job_posting_year_month,
b.job_postings_count
from
skill_variables as a
inner join
(
  select
  'skill' as variable_type,
  *
  from (
  select ALPHA_2_CODE as geography, substring(replace(DATE::string,'-'),1,6) as job_posting_year_month, skill_name_en_us as variable_name, count(distinct vacancy_key) as job_postings_count

    from

    (select vacancy_key,skill.value as BGT_SKILL_030401_KEY, FIRST_ACTIVE_DATE_KEY, loc.value as loc_key

  from "BGT_DATA_34"."PUBLIC"."F_BGT_VACANCY",

  lateral flatten(input => F_BGT_VACANCY.LOCATION_010000_KEY) loc,

  lateral flatten(input => F_BGT_VACANCY.BGT_SKILL_030401_KEY) skill) vacancies

  INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_LOCATION_010000') as D_LOCATION_010000

              ON vacancies.loc_key = D_LOCATION_010000.LOCATION_010000_KEY

  INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_DATE') as DATE

              ON vacancies.FIRST_ACTIVE_DATE_KEY = DATE.DATE_KEY

  INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_BGT_SKILL_030401') as SKILL

              ON vacancies.BGT_SKILL_030401_KEY = SKILL.BGT_SKILL_030401_KEY

  where ALPHA_2_CODE IN ('US','CA','GB','AU','NZ','SG','ID')

  group by ALPHA_2_CODE, substring(replace(DATE::string,'-'),1,6), skill_name_en_us

  order by ALPHA_2_CODE, substring(replace(DATE::string,'-'),1,6), skill_name_en_us)

  union all
  select
  'skill' as variable_type,
  'all' as geography,
  *
  from

  (
  select substring(replace(DATE::string,'-'),1,6) as job_posting_year_month, skill_name_en_us as variable_name, count(distinct vacancy_key) as job_postings_count

    from

    (select vacancy_key,skill.value as BGT_SKILL_030401_KEY, FIRST_ACTIVE_DATE_KEY, loc.value as loc_key

  from "BGT_DATA_34"."PUBLIC"."F_BGT_VACANCY",

  lateral flatten(input => F_BGT_VACANCY.LOCATION_010000_KEY) loc,

  lateral flatten(input => F_BGT_VACANCY.BGT_SKILL_030401_KEY) skill) vacancies

  INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_LOCATION_010000') as D_LOCATION_010000

              ON vacancies.loc_key = D_LOCATION_010000.LOCATION_010000_KEY

  INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_DATE') as DATE

              ON vacancies.FIRST_ACTIVE_DATE_KEY = DATE.DATE_KEY

  INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_BGT_SKILL_030401') as SKILL

              ON vacancies.BGT_SKILL_030401_KEY = SKILL.BGT_SKILL_030401_KEY

  where ALPHA_2_CODE IN ('US','CA','GB','AU','NZ','SG','ID')

  group by substring(replace(DATE::string,'-'),1,6), skill_name_en_us

  order by  substring(replace(DATE::string,'-'),1,6), skill_name_en_us)


union all
select
'skill_cluster' as variable_type,
*
from (
select ALPHA_2_CODE as geography, substring(replace(DATE::string,'-'),1,6) as job_posting_year_month, skill_cluster_name_en_us as variable_name, count(distinct vacancy_key) as job_postings_count

  from

  (select vacancy_key,skill.value as BGT_SKILL_030401_KEY, FIRST_ACTIVE_DATE_KEY, loc.value as loc_key

from "BGT_DATA_34"."PUBLIC"."F_BGT_VACANCY",

lateral flatten(input => F_BGT_VACANCY.LOCATION_010000_KEY) loc,

lateral flatten(input => F_BGT_VACANCY.BGT_SKILL_030401_KEY) skill) vacancies

INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_LOCATION_010000') as D_LOCATION_010000

            ON vacancies.loc_key = D_LOCATION_010000.LOCATION_010000_KEY

INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_DATE') as DATE

            ON vacancies.FIRST_ACTIVE_DATE_KEY = DATE.DATE_KEY

INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_BGT_SKILL_030401') as SKILL

            ON vacancies.BGT_SKILL_030401_KEY = SKILL.BGT_SKILL_030401_KEY

where ALPHA_2_CODE IN ('US','CA','GB','AU','NZ','SG','ID')

group by ALPHA_2_CODE, substring(replace(DATE::string,'-'),1,6), skill_cluster_name_en_us

order by ALPHA_2_CODE, substring(replace(DATE::string,'-'),1,6), skill_cluster_name_en_us)

union all
select
'skill_cluster' as variable_type,
'all' as geography,
*
from

(
select substring(replace(DATE::string,'-'),1,6) as job_posting_year_month, skill_cluster_name_en_us as variable_name, count(distinct vacancy_key) as job_postings_count

  from

  (select vacancy_key,skill.value as BGT_SKILL_030401_KEY, FIRST_ACTIVE_DATE_KEY, loc.value as loc_key

from "BGT_DATA_34"."PUBLIC"."F_BGT_VACANCY",

lateral flatten(input => F_BGT_VACANCY.LOCATION_010000_KEY) loc,

lateral flatten(input => F_BGT_VACANCY.BGT_SKILL_030401_KEY) skill) vacancies

INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_LOCATION_010000') as D_LOCATION_010000

            ON vacancies.loc_key = D_LOCATION_010000.LOCATION_010000_KEY

INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_DATE') as DATE

            ON vacancies.FIRST_ACTIVE_DATE_KEY = DATE.DATE_KEY

INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_BGT_SKILL_030401') as SKILL

            ON vacancies.BGT_SKILL_030401_KEY = SKILL.BGT_SKILL_030401_KEY

where ALPHA_2_CODE IN ('US','CA','GB','AU','NZ','SG','ID')

group by substring(replace(DATE::string,'-'),1,6), skill_cluster_name_en_us

order by  substring(replace(DATE::string,'-'),1,6), skill_cluster_name_en_us)

union all
select
'skill_cluster_family' as variable_type,
*
from (
select ALPHA_2_CODE as geography, substring(replace(DATE::string,'-'),1,6) as job_posting_year_month, SKILL_CLUSTER_FAMILY_NAME_EN_US as variable_name, count(distinct vacancy_key) as job_postings_count

  from

  (select vacancy_key,skill.value as BGT_SKILL_030401_KEY, FIRST_ACTIVE_DATE_KEY, loc.value as loc_key

from "BGT_DATA_34"."PUBLIC"."F_BGT_VACANCY",

lateral flatten(input => F_BGT_VACANCY.LOCATION_010000_KEY) loc,

lateral flatten(input => F_BGT_VACANCY.BGT_SKILL_030401_KEY) skill) vacancies

INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_LOCATION_010000') as D_LOCATION_010000

            ON vacancies.loc_key = D_LOCATION_010000.LOCATION_010000_KEY

INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_DATE') as DATE

            ON vacancies.FIRST_ACTIVE_DATE_KEY = DATE.DATE_KEY

INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_BGT_SKILL_030401') as SKILL

            ON vacancies.BGT_SKILL_030401_KEY = SKILL.BGT_SKILL_030401_KEY

where ALPHA_2_CODE IN ('US','CA','GB','AU','NZ','SG','ID')

group by ALPHA_2_CODE, substring(replace(DATE::string,'-'),1,6), SKILL_CLUSTER_FAMILY_NAME_EN_US

order by ALPHA_2_CODE, substring(replace(DATE::string,'-'),1,6), SKILL_CLUSTER_FAMILY_NAME_EN_US)

union all
select
'skill_cluster_family' as variable_type,
'all' as geography,
*
from

(
select substring(replace(DATE::string,'-'),1,6) as job_posting_year_month, SKILL_CLUSTER_FAMILY_NAME_EN_US as variable_name, count(distinct vacancy_key) as job_postings_count

  from

  (select vacancy_key,skill.value as BGT_SKILL_030401_KEY, FIRST_ACTIVE_DATE_KEY, loc.value as loc_key

from "BGT_DATA_34"."PUBLIC"."F_BGT_VACANCY",

lateral flatten(input => F_BGT_VACANCY.LOCATION_010000_KEY) loc,

lateral flatten(input => F_BGT_VACANCY.BGT_SKILL_030401_KEY) skill) vacancies

INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_LOCATION_010000') as D_LOCATION_010000

            ON vacancies.loc_key = D_LOCATION_010000.LOCATION_010000_KEY

INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_DATE') as DATE

            ON vacancies.FIRST_ACTIVE_DATE_KEY = DATE.DATE_KEY

INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_BGT_SKILL_030401') as SKILL

            ON vacancies.BGT_SKILL_030401_KEY = SKILL.BGT_SKILL_030401_KEY

where ALPHA_2_CODE IN ('US','CA','GB','AU','NZ','SG','ID')

group by substring(replace(DATE::string,'-'),1,6), SKILL_CLUSTER_FAMILY_NAME_EN_US

order by  substring(replace(DATE::string,'-'),1,6), SKILL_CLUSTER_FAMILY_NAME_EN_US)


)
as b on a.geography = b.geography
  and a.variable_type = b.variable_type
  and a.variable_name = b.variable_name
),

max_year_month as
(
select
201401 as first_year_month,
100 * year(dateadd(month, -1, CURRENT_DATE()))
  + month(dateadd(month, -1, CURRENT_DATE())) as
  last_year_month
),

subset as
(
select
b.variable_id,
a.year_month as job_posting_year_month,
(case when c.job_postings_count is not null then c.job_postings_count
  else 0 end)::number(38, 0) as job_postings_count
from
(select distinct year_month from
 date_dimension) as a
inner join max_year_month as d on true
inner join (select distinct variable_id from skill_variables)
  as b on true
left join base_table as c
on a.year_month = c.job_posting_year_month
  and b.variable_id = c.variable_id
where a.year_month >= d.first_year_month and
  a.year_month <= d.last_year_month
),

adjusted as
(
select
a.*,
(case when b.job_postings_count is not null
  and b.job_postings_count > 0 then
  a.job_postings_count / b.job_postings_count else
  0 end)::float as job_postings_count_adjusted
from
subset as a
left join
(select  (case when month(job_date) in ('10', '11', '12') then concat_ws('', year(job_date), month(job_date)) else concat_ws('', year(job_date), '0', month(job_date)) end) as job_posting_year_month, count(*) as job_postings_count
   FROM loc_date as ld
     group by job_posting_year_month) as b
on a.job_posting_year_month = b.job_posting_year_month),

deviation as
(
select
*,
(avg(job_postings_count) over (partition by variable_id
  order by job_posting_year_month rows between 3 preceding and 3 following))
  ::float as rolling_average,
(case when rolling_average > 0 then
  job_postings_count / rolling_average - 1
  else case when job_postings_count > 0 then 10000 else null end end)
  ::float as relative_deviation_from_rolling_average
from
adjusted
)
select
*
from
deviation
