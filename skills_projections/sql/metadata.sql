/*
Last Modification Date: 2021-01-25
Author: Gilberto Noronha <gnoronha@burning-glass.com>

Please see the "README" file for basic installation and usage
instructions.

*/

select
a.variable_id as prediction_id,
a.variable_id as variable_id_bgt,
b.variable_id as variable_id_stack_overflow,
a.geography,
a.variable_type,
a.variable_name,
a.variability as variability_bgt,
a.sparsity as sparsity_bgt,
a.availability as availability_bgt,
a.reliability as reliability_bgt,
a.first_valid_year_month as first_valid_year_month_bgt,
a.passed_quality_checks as use_bgt_data,
b.variability as variability_stack_overflow,
b.sparsity as sparsity_stack_overflow,
b.availability as availability_stack_overflow,
b.reliability as reliability_stack_overflow,
b.first_valid_year_month as first_valid_year_month_stack_overflow,
case when b.passed_quality_checks is not null and
  b.passed_quality_checks then b.passed_quality_checks else
  false end as use_stack_overflow_data,
case when a.passed_quality_checks and b.passed_quality_checks then
  greatest(a.first_valid_year_month, b.first_valid_year_month) else
  a.first_valid_year_month end as first_used_year_month_original,
case when first_used_year_month_original is null then e.first_valid_date else first_used_year_month_original end first_used_year_month,
a.passed_quality_checks as passed_quality_checks,
c.adjustment as market_growth_adjustment,
d.avg_posting_count,
e.first_valid_date
from
skill_quality_metrics as a
left join
  identifier($skill_predictor_stack_overflow_skill_quality_metrics_table)
  as b
on a.variable_type = b.variable_type
  and a.variable_name = b.variable_name
inner join BGT_TEMPORARY_DATA.GSINAPI.MARKET_GROWTH_ADJUSTMENT as c
  on a.geography = c.geography
inner join (select variable_id, avg(job_postings_count) as avg_posting_count
     from monthly_skill_counts
     group by variable_id) as d
  on a.variable_id = d.variable_id
  left join (select variable_id, min(job_posting_year_month) as first_valid_date from monthly_skill_counts
       where abs(relative_deviation_from_rolling_average) <= 0.3 and job_postings_count > 2
      group by variable_id
   ) as e
       on a.variable_id = e.variable_id
