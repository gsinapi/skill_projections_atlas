/*
Last Modification Date: 2021-01-11
Author: Gilberto Noronha <gnoronha@burning-glass.com>

Please see the "README" file for basic installation and usage
instructions.

3rd quartile: percentile_cont(0.75) within group (order by aggregate_index) as base_value
95 perc quartile: percentile_cont(0.95) within group (order by aggregate_index) as base_value
avg: avg(aggregate_index)

*/

select
a.prediction_id,
greatest(power(1 + $skill_predictor_svr_lower_bound, 1 / 5) - 1
  + uniform(-30000, 30000, random($skill_predictor_seed)) / 1000000,
  b.one_year_growth_base)::float as
  one_year_growth,
greatest(power(1 + $skill_predictor_svr_lower_bound, 2 / 5) - 1
  + uniform(-30000, 30000, random($skill_predictor_seed + 1)) / 1000000,
case when (one_year_growth < 0 and b.two_years_growth_base
  > one_year_growth) or (one_year_growth >= 0 and
  b.two_years_growth_base < one_year_growth) then
  power(1 + one_year_growth, 2) - 1 else
  b.two_years_growth_base end)::float
  as two_years_growth,
greatest(power(1 + $skill_predictor_svr_lower_bound, 3 / 5) - 1
  + uniform(-30000, 30000, random($skill_predictor_seed + 2)) / 1000000,
case when (two_years_growth < 0 and b.three_years_growth_base
  > two_years_growth) or (two_years_growth >= 0 and
  b.three_years_growth_base < two_years_growth) then
  power(1 + two_years_growth, 3 / 2) - 1 else
  b.three_years_growth_base end)::float
  as three_years_growth,
greatest(power(1 + $skill_predictor_svr_lower_bound, 5 / 5) - 1
  + uniform(-30000, 30000, random($skill_predictor_seed + 3)) / 1000000,
case when (three_years_growth < 0 and b.five_years_growth_base
  > three_years_growth) or (three_years_growth >= 0 and
  b.five_years_growth_base < three_years_growth) then
  power(1 + three_years_growth, 5 / 3) - 1 else
  b.five_years_growth_base end)::float
  as five_years_growth
from
metadata as a
inner join
(select
distinct
prediction_id,
nth_value(growth_rate, 12) over (partition by prediction_id
  order by year_month) as one_year_growth_base,
nth_value(growth_rate, 24) over (partition by prediction_id
   order by year_month) as two_years_growth_base,
nth_value(growth_rate, 36) over (partition by prediction_id
   order by year_month) as three_years_growth_base,
nth_value(growth_rate, 60) over (partition by prediction_id
   order by year_month) as five_years_growth_base
from
(select
a.prediction_id,
a.year_month,
div0(a.predicted, b.base_value) - 1 as growth_rate
from
predicted_level_svr as a
inner join
(
select
prediction_id,
percentile_cont(0.95) within group (order by aggregate_index) as base_value
from
prepared_input
group by prediction_id) as b
on a.prediction_id = b.prediction_id)) as b
on a.prediction_id = b.prediction_id
where one_year_growth is not null
