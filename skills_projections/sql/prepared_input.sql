/*
Last Modification Date: 2021-01-09
Author: Gilberto Noronha <gnoronha@burning-glass.com>

Please see the "README" file for basic installation and usage
instructions.

where (b.passed_quality_checks or b.reliability_bgt >= 0.3 or b.avg_posting_count > 1) and year_month >= b.first_used_year_month

all skills: (b.passed_quality_checks in (True, False) or b.reliability_bgt >= 0.3)
backtesting add:  where a.year_month <202000
*/

with bgt_counts as
(
select
a.variable_id,
a.job_posting_year_month as year_month,
(case when b.first_year_month_count > 0 then
  100 * a.job_postings_count_adjusted / b.first_year_month_count
  else 10000 end)::float as bgt_index
from monthly_skill_counts as a
inner join
(select
b.variable_id_bgt,
b.geography,
b.variable_type,
b.variable_name,
a.job_postings_count_adjusted as first_year_month_count,
b.first_used_year_month,
b.passed_quality_checks,
b.avg_posting_count,
b.reliability_bgt
from monthly_skill_counts as a
inner join metadata as b
on a.variable_id = b.variable_id_bgt
  and a.job_posting_year_month = b.first_used_year_month) as b
on a.variable_id = b.variable_id_bgt
where (b.passed_quality_checks or b.reliability_bgt >= 0.3) and year_month >= b.first_used_year_month
),

stack_overflow_counts as
(
select
b.variable_id_bgt,
a.question_year_month as year_month,
(case when b.first_year_month_count > 0 then
  100 * a.questions_count_adjusted / b.first_year_month_count
  else 10000 end)::float as stack_overflow_index
from
identifier($skill_predictor_stack_overflow_monthly_skill_counts_table) as a
inner join
(select
b.variable_id_bgt,
b.variable_id_stack_overflow,
a.questions_count_adjusted as first_year_month_count,
b.first_used_year_month,
b.passed_quality_checks,
b.avg_posting_count,
b.reliability_bgt
from
identifier($skill_predictor_stack_overflow_monthly_skill_counts_table) as a
inner join (select * from metadata where
   use_stack_overflow_data) as b
on a.variable_id = b.variable_id_stack_overflow
  and a.question_year_month = b.first_used_year_month) as b
on a.variable_id = b.variable_id_stack_overflow
where (b.passed_quality_checks or b.reliability_bgt >= 0.3) and year_month >= b.first_used_year_month
)

select
a.variable_id as prediction_id,
a.year_month,
a.bgt_index,
b.stack_overflow_index,
(case when b.stack_overflow_index is not null then
  (2 * a.bgt_index + b.stack_overflow_index) / 3
  else a.bgt_index end)::float as
  aggregate_index
from
bgt_counts as a
left join stack_overflow_counts as b
  on a.variable_id = b.variable_id_bgt
  and a.year_month = b.year_month
