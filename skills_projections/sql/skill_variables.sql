/*
Last Modification Date: 2021-01-19
Author: Gilberto Noronha <gnoronha@burning-glass.com>

Please see the "README" file for basic installation and usage
instructions.

*/

with general_table as (
select skill_name_en_us as variable_name,'skill' as variable_type,  skill_id, skill_name_en_us as skill,
skill_cluster_name_en_us as skill_cluster, skill_cluster_family_name_en_us as skill_cluster_family
from BGT_DATA_34.PUBLIC.D_BGT_SKILL_CURRENT_PRODUCTION
where skill_name_en_us is not null
union all
select distinct(skill_cluster_name_en_us) as variable_name,'skill_cluster' as variable_type,  'NULL' as skill_id, 'NULL' as skill,
skill_cluster_name_en_us as skill_cluster, skill_cluster_family_name_en_us as skill_cluster_family
from BGT_DATA_34.PUBLIC.D_BGT_SKILL_CURRENT_PRODUCTION
where skill_cluster_name_en_us is not null
union all
select distinct(skill_cluster_family_name_en_us) as variable_name,'skill_cluster_family' as variable_type,  'NULL' as skill_id, 'NULL' as skill,
'NULL' as skill_cluster, skill_cluster_family_name_en_us as skill_cluster_family
from BGT_DATA_34.PUBLIC.D_BGT_SKILL_CURRENT_PRODUCTION
where skill_cluster_family_name_en_us is not null),
skill_variables_atlas as (
select 'US' as geography, a.* from general_table as a
union all
select 'GB' as geography, a.* from general_table as a
union all
select 'CA' as geography, a.* from general_table as a
union all
select 'AU' as geography, a.* from general_table as a
union all
select 'SG' as geography, a.* from general_table as a
union all
select 'NZ' as geography, a.* from general_table as a
union all
select 'all' as geography, a.* from general_table as a)
select a.*, row_number()
  over (order by geography desc) as variable_id
  from skill_variables_atlas as a     
