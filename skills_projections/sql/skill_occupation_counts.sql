/*
Last Modification Date: 2021-01-09
Author: Gilberto Noronha <gnoronha@burning-glass.com>

Please see the "README" file for basic installation and usage
instructions.

*/

select
a.prediction_id,
b.occupation,
b.occupation_rank,
b.job_postings_count,
b.occupation_one_year_growth,
b.occupation_two_years_growth,
b.occupation_three_years_growth,
b.occupation_five_years_growth,
c.one_year_growth,
c.two_years_growth,
c.three_years_growth,
c.five_years_growth,
case when one_year_growth is null then 1 else
  case when occupation_one_year_growth >= 0 and
    one_year_growth >= 0 or occupation_one_year_growth <= 0
    and one_year_growth <= 0 then least((abs(abs(one_year_growth) /
    abs(occupation_one_year_growth) - 1) / 20), 1) else 1 end end
  as svr_prediction_deviation
from
metadata as a
inner join
(
select
a.*,
b.one_year_growth / 100 as occupation_one_year_growth,
b.two_year_growth / 100 as occupation_two_years_growth,
b.three_year_growth / 100 as occupation_three_years_growth,
b.five_year_growth / 100 as occupation_five_years_growth,
'skill' as variable_type
from
(select
*,
dense_rank() over (partition by geography, variable_name
  order by job_postings_count desc) as occupation_rank
from
(select ALPHA_2_CODE as geography, OCCUPATION_NAME_EN_US as occupation, SKILL_NAME_EN_US as variable_name, count(distinct vacancy_key) as job_postings_count

  from

  (select vacancy_key,skill.value as BGT_SKILL_030401_KEY, FIRST_ACTIVE_DATE_KEY, loc.value as loc_key,BGT_OCCUPATION_030401_KEY

from "BGT_DATA_34"."PUBLIC"."F_BGT_VACANCY",

lateral flatten(input => F_BGT_VACANCY.LOCATION_010000_KEY) loc,

lateral flatten(input => F_BGT_VACANCY.BGT_SKILL_030401_KEY) skill) vacancies

INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_LOCATION_010000') as D_LOCATION_010000

            ON vacancies.loc_key = D_LOCATION_010000.LOCATION_010000_KEY

INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_DATE') as DATE

            ON vacancies.FIRST_ACTIVE_DATE_KEY = DATE.DATE_KEY

INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_BGT_SKILL_030401') as SKILL

            ON vacancies.BGT_SKILL_030401_KEY = SKILL.BGT_SKILL_030401_KEY

INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_BGT_OCCUPATION_030401') as OCC

            ON vacancies.BGT_OCCUPATION_030401_KEY = OCC.BGT_OCCUPATION_030401_KEY

where ALPHA_2_CODE IN ('US','CA','GB','AU','NZ','SG','ID')

and DATE.DATE<=current_date and DATE.DATE>dateadd(day,-365,current_date)

group by ALPHA_2_CODE, OCCUPATION_NAME_EN_US, SKILL_NAME_EN_US

order by ALPHA_2_CODE, OCCUPATION_NAME_EN_US, SKILL_NAME_EN_US )) as a
inner join (select * from bgt_intelligent_dataset.projection.occupation
 where geography_type = 'Nationwide') as b
on a.occupation = b.occupation_name_en_us

union all
select
a.*,
b.one_year_growth / 100 as occupation_one_year_growth,
b.two_year_growth / 100 as occupation_two_years_growth,
b.three_year_growth / 100 as occupation_three_years_growth,
b.five_year_growth / 100 as occupation_five_years_growth,
'skill_cluster' as variable_type
from
(select
*,
dense_rank() over (partition by geography, variable_name
  order by job_postings_count desc) as occupation_rank
from
(select ALPHA_2_CODE as geography, OCCUPATION_NAME_EN_US as occupation, SKILL_CLUSTER_NAME_EN_US as variable_name, count(distinct vacancy_key) as job_postings_count

  from

  (select vacancy_key,skill.value as BGT_SKILL_030401_KEY, FIRST_ACTIVE_DATE_KEY, loc.value as loc_key,BGT_OCCUPATION_030401_KEY

from "BGT_DATA_34"."PUBLIC"."F_BGT_VACANCY",

lateral flatten(input => F_BGT_VACANCY.LOCATION_010000_KEY) loc,

lateral flatten(input => F_BGT_VACANCY.BGT_SKILL_030401_KEY) skill) vacancies

INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_LOCATION_010000') as D_LOCATION_010000

            ON vacancies.loc_key = D_LOCATION_010000.LOCATION_010000_KEY

INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_DATE') as DATE

            ON vacancies.FIRST_ACTIVE_DATE_KEY = DATE.DATE_KEY

INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_BGT_SKILL_030401') as SKILL

            ON vacancies.BGT_SKILL_030401_KEY = SKILL.BGT_SKILL_030401_KEY

INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_BGT_OCCUPATION_030401') as OCC

            ON vacancies.BGT_OCCUPATION_030401_KEY = OCC.BGT_OCCUPATION_030401_KEY

where ALPHA_2_CODE IN ('US','CA','GB','AU','NZ','SG','ID')

and DATE.DATE<=current_date and DATE.DATE>dateadd(day,-365,current_date)

group by ALPHA_2_CODE, OCCUPATION_NAME_EN_US, SKILL_CLUSTER_NAME_EN_US

order by ALPHA_2_CODE, OCCUPATION_NAME_EN_US, SKILL_CLUSTER_NAME_EN_US )) as a
inner join (select * from bgt_intelligent_dataset.projection.occupation
 where geography_type = 'Nationwide') as b
on a.occupation = b.occupation_name_en_us

union all
select
a.*,
b.one_year_growth / 100 as occupation_one_year_growth,
b.two_year_growth / 100 as occupation_two_years_growth,
b.three_year_growth / 100 as occupation_three_years_growth,
b.five_year_growth / 100 as occupation_five_years_growth,
'skill_cluster_family' as variable_type
from
(select
*,
dense_rank() over (partition by geography, variable_name
  order by job_postings_count desc) as occupation_rank
from
(select ALPHA_2_CODE as geography, OCCUPATION_NAME_EN_US as occupation, SKILL_CLUSTER_FAMILY_NAME_EN_US as variable_name, count(distinct vacancy_key) as job_postings_count

  from

  (select vacancy_key,skill.value as BGT_SKILL_030401_KEY, FIRST_ACTIVE_DATE_KEY, loc.value as loc_key,BGT_OCCUPATION_030401_KEY

from "BGT_DATA_34"."PUBLIC"."F_BGT_VACANCY",

lateral flatten(input => F_BGT_VACANCY.LOCATION_010000_KEY) loc,

lateral flatten(input => F_BGT_VACANCY.BGT_SKILL_030401_KEY) skill) vacancies

INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_LOCATION_010000') as D_LOCATION_010000

            ON vacancies.loc_key = D_LOCATION_010000.LOCATION_010000_KEY

INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_DATE') as DATE

            ON vacancies.FIRST_ACTIVE_DATE_KEY = DATE.DATE_KEY

INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_BGT_SKILL_030401') as SKILL

            ON vacancies.BGT_SKILL_030401_KEY = SKILL.BGT_SKILL_030401_KEY

INNER JOIN IDENTIFIER('BGT_DATA_34.PUBLIC.D_BGT_OCCUPATION_030401') as OCC

            ON vacancies.BGT_OCCUPATION_030401_KEY = OCC.BGT_OCCUPATION_030401_KEY

where ALPHA_2_CODE IN ('US','CA','GB','AU','NZ','SG','ID')

and DATE.DATE<=current_date and DATE.DATE>dateadd(day,-365,current_date)

group by ALPHA_2_CODE, OCCUPATION_NAME_EN_US, SKILL_CLUSTER_FAMILY_NAME_EN_US

order by ALPHA_2_CODE, OCCUPATION_NAME_EN_US, SKILL_CLUSTER_FAMILY_NAME_EN_US )) as a
inner join (select * from bgt_intelligent_dataset.projection.occupation
 where geography_type = 'Nationwide') as b
on a.occupation = b.occupation_name_en_us
) as b on a.geography = b.geography and a.variable_name = b.variable_name
and a.variable_type = b.variable_type
left join predicted_growth_svr as c
on a.prediction_id = c.prediction_id
