#!/usr/bin/env python
#
# Last Modification Date: 2021-01-08
# Author: Gilberto Noronha <gnoronha@burning-glass.com> 
#
# Please see the "README" file for basic installation and usage
# instructions.
#

import logging

logging.getLogger(__name__).addHandler(logging.NullHandler())

from snowflake_helpers.utilities import make_directory\
                                      , make_logger\
                                      , iterate

from .skill_growth_predictor import Skill_growth_predictor
