#!/usr/bin/env python
#
# Last Modification Date: 2021-01-11
# Author: Gilberto Noronha <gnoronha@burning-glass.com>
#
# Please see the "README" file for basic installation and usage
# instructions.
#

from .utilities import year_month_add
from .svm import train_and_predict_many
from snowflake_helpers.utilities import make_directory
from snowflake_helpers import sql_to_snowflake
import snowflake_helpers as snow
import pyarrow.parquet as pq
import pyarrow as pa
import pandas as pd
import numpy as np
import os
import pathlib
import dateutil
import datetime
import logging

logger = logging.getLogger(__name__)


class Skill_growth_predictor:

    def log(self, message):
        logger.info("Skill_growth_predictor: {}".format(message))

    def _abspath(self, fname, extension=None):

        if extension is None:
            extension = self._default_extension

        return os.path.join(self._cache_directory, fname + extension)

    def _exists(self, fname):
        return os.path.isfile(self._abspath(fname))

    def _log_saved(self, fname):
        self.log("Data was saved to file: \"{}\"."
                 .format(self._abspath(fname)))

    def _log_existing(self, fname):
        self.log("Using existing data file: \"{}\"."
                 .format(self._abspath(fname)))

    def _load(self,
              fname,
              to_pandas=False):

        result = pq.read_table(self._abspath(fname))

        self._log_existing(fname)

        if to_pandas:
            return result.to_pandas()

        return result

    def _save(self, table, fname):

        pq.write_table(table, self._abspath(fname))

        self._log_saved(fname)

    def _close_connection(self):
        if self._connection is not None:
            self._connection.close()

    def connect(self, reconnect=None):

        if reconnect is None:
            reconnect = True

        if self._connection is None or\
                self._connection.is_closed() or reconnect:

            self._close_connection()

            self._connection = snow.connect(**self._connection_kwargs)

            with self._connection.cursor() as cursor:

                for key, value in self._sql_variables.items():

                    string = value
                    if isinstance(value, str):
                        string = "'{}'".format(value)

                    cursor.execute("set skill_predictor_{} = {}"
                                   .format(key, string))

        return self._connection

    def arrow_to_snowflake(self,
                           data,
                           output_table):

        connection = self.connect()

        output_table = "{}.{}"\
            .format(self._development_schema,
                    output_table)

        snow.arrow_to_snowflake(connection,
                                data,
                                output_table,
                                if_exists="replace")

    def _from_snowflake_impl(self,
                             function,
                             sql,
                             schema=None):

        connection = self.connect()

        if schema is not None:
            with connection.cursor() as cursor:
                cursor.execute("use schema {}".format(schema))

        return function(connection, sql)

    def from_snowflake(self,
                       sql,
                       schema=None):

        return self._from_snowflake_impl(snow.from_snowflake,
                                         sql,
                                         schema)

    def snowflake_to_arrow(self,
                           sql,
                           schema=None):

        return self._from_snowflake_impl(snow.snowflake_to_arrow,
                                         sql,
                                         schema)

    def _get_data_impl(self,
                       function,
                       source,
                       use_cache=True,
                       to_pandas=False):

        def helper(result):

            self._data[source] = result

            if to_pandas:
                return result.to_pandas()

            return result

        cache_file = "{}".format(source)

        if use_cache:

            if source in self._data:
                return helper(self._data[source])

            elif self._exists(cache_file):
                return helper(self._load(cache_file))

            else:

                result = function()

                self._save(result, source)

                return helper(result)

        else:

            result = function()

            self._save(result, source)

            return helper(result)

    def get_data(self,
                 source,
                 use_cache=True,
                 to_pandas=False):

        return self._get_data_impl(
            self._impl[source],
            source,
            use_cache,
            to_pandas)

    def _materialize_tables_impl(self, table):

        sql_tail = self._order_by.get(table, "")

        if sql_tail != "":
            sql_tail = "order by {}".format(", "
                                            .join(sql_tail))

        sql_file = os.path.join(self._sql_directory,
                                "{}.sql".format(table))

        with open(sql_file, "r") as f:

            sql = "{} {}".format(f.read(), sql_tail)

            connection = self.connect()

            with connection.cursor() as cursor:
                cursor.execute("use schema {}".format(self._development_schema))

            destination_table = "{}.{}".format(self._development_schema, table)

            sql_to_snowflake(connection, connection, sql,
                             destination_table, if_exists="replace")

    def _materialize_tables(self, tables):
        for t in tables:
            self._materialize_tables_impl(t)

    def _predict_growth_derived(self):

        self._materialize_tables(["occupation_defining_skill",
                                  "skill_occupation_counts",
                                  "predicted_growth_derived"])

        # sql = "select * from predicted_growth_derived order by {}"\
        #           .format(self._prediction_id_column)

        sql = "select * from {}.predicted_growth_derived order by {}"\
            .format(self._development_schema, self._prediction_id_column)

        return self.snowflake_to_arrow(sql)

    def _prepare_input_data(self):
        self._materialize_tables(["metadata", "prepared_input"])

    def _prepare_skill_variables(self):
        self._materialize_tables(["skill_variables"])

    def _prepare_monthly_skill_counts(self):
        self._materialize_tables(["monthly_skill_counts"])

    def _prepare_skill_quality_metrics(self):
        self._materialize_tables(["skill_quality_metrics"])

    def _get_svr_input_impl(self):

        self._prepare_input_data()

        sql = (("select * from {} order by {}"
                ", year_month"))\
            .format(self._svr_input_table,
                    self._prediction_id_column)

        return self.snowflake_to_arrow(sql)

    def _train_and_predict(self,
                           output_column=None,
                           use_logarithm=True,
                           processes=None):

        input_data = self.get_data("svr_input",
                                   True,
                                   True)

        max_year_month = input_data["year_month"].max()

        prediction_start_date = year_month_add(max_year_month, 1)
        prediction_end_date = year_month_add(max_year_month,
                                             self._prediction_months)

        result = train_and_predict_many(
            input_data,
            ddof=self._svr_ddof,
            prediction_id_column=self._prediction_id_column,
            date_column=self._date_column,
            value_column=self._value_column,
            prediction_start_date=prediction_start_date,
            prediction_end_date=prediction_end_date,
            svr_kwargs=self._svr_kwargs,
            output_column=output_column,
            use_logarithm=use_logarithm)

        return pa.Table.from_pandas(result)

    def _predict_growth_svr(self):

        data = self.get_data("svr_output")

        self.arrow_to_snowflake(data,
                                "predicted_level_svr")

        connection = self.connect()

        self._materialize_tables(["predicted_growth_svr"])

        sql = "select * from {}.predicted_growth_svr"\
            .format(self._development_schema)

        return self.snowflake_to_arrow(sql)

    def _predict_growth(self):

        self.get_data("predicted_growth_svr")

        self.get_data("predicted_growth_derived")

        self._materialize_tables(["predicted_growth"])

        sql = "select * from {}.predicted_growth"\
            .format(self._development_schema)

        return self.snowflake_to_arrow(sql)

    def __init__(self,
                 development_schema,
                 cache_directory=None,
                 bgt_schema=None,
                 stack_overflow_schema=None,
                 connection_kwargs=None):

        if cache_directory is None:
            cache_directory = ".skill_growth_predictor"

        if bgt_schema is None:
            bgt_schema = "bgt_derived_data.optimized_tables"

        if stack_overflow_schema is None:
            stack_overflow_schema = "bgt_stack_overflow.specialized"

        self._development_schema = development_schema

        self._cache_directory = os.path.realpath(cache_directory)

        self._source_schemas = {"metadata": bgt_schema,
                                "bgt": bgt_schema,
                                "stack_overflow": stack_overflow_schema}

        self._connection_kwargs = connection_kwargs

        self._sql_directory = os.path.join(pathlib.Path(__file__)
                                           .parent.absolute(), "sql")

        self._connection = None

        make_directory(self._cache_directory)

        self._default_extension = ".parquet"

        self._raw_data = {}

        self._data = {}

        self._prediction_id_column = "prediction_id"
        self._date_column = "year_month"
        self._value_column = "aggregate_index"

        self._order_by = {
            "occupation_defining_skill": [self._prediction_id_column,
                                          "occupation"],
            "predicted_growth": [self._prediction_id_column],
            "predicted_growth_svr": [self._prediction_id_column],
            "prepared_input": [self._prediction_id_column,
                               "year_month"],
            "metadata": [self._prediction_id_column],
            "skill_occupation_counts":
            [self._prediction_id_column,
             "occupation"],
            "predicted_growth_derived":
            [self._prediction_id_column]
        }

        self._svr_kwargs = {"gamma": 0.005,
                            "C": 25,
                            "kernel": "rbf",
                            "scale": True}

        self._svr_input_table = "{}.prepared_input"\
            .format(self._development_schema)

        self._impl = {
            "svr_input": self._get_svr_input_impl,
            "svr_output": self._train_and_predict,
            "predicted_growth_svr": self._predict_growth_svr,
            "predicted_growth_derived": self._predict_growth_derived,
            "predicted_growth": self._predict_growth
        }

        self._prediction_months = 120
        self._svr_prediction_deviation_cutoff = 0.16
        self._svr_lower_bound = -0.3
        self._seed = 12

        self._sql_variables = {
            "bgt_skill_quality_metrics_table":
            "{}.skill_quality_metrics".format(self._source_schemas["bgt"]),
            "stack_overflow_skill_quality_metrics_table":
            "{}.skill_quality_metrics"
            .format(self._source_schemas["stack_overflow"]),
            "bgt_monthly_skill_counts_table":
            "{}.monthly_skill_counts".format(self._source_schemas["bgt"]),
            "stack_overflow_monthly_skill_counts_table":
            "{}.monthly_skill_counts"
            .format(self._source_schemas["stack_overflow"]),
            "job_posting_table":
            "{}.job_posting".format(self._source_schemas["bgt"]),
            "svr_prediction_deviation_cutoff":
            self._svr_prediction_deviation_cutoff,
            "svr_lower_bound": self._svr_lower_bound,
            "seed": self._seed
        }

        self._svr_ddof = 1
