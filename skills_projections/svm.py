#!/usr/bin/env python
#
# Last Modification Date: 2021-01-09
# Author: Gilberto Noronha <gnoronha@burning-glass.com>
#
# Please see the "README" file for basic installation and usage
# instructions.
#

from .utilities import encode_dates
from snowflake_helpers.utilities import iterate
from sklearn.svm import SVR
import pandas as pd
import numpy as np
import logging

logger = logging.getLogger(__name__)


def standard_scale(x, ddof=0):

    m = np.mean(x)

    s = np.std(x, ddof=ddof)

    return (x - m) / s, m, s


def scale(x, m=0, s=1):
    return (x - m) / s


def scale_back(x, m=0, s=1):
    return x * s + m


class Svr:

    def __init__(self,
                 kernel=None,
                 gamma=None,
                 C=None,
                 scale=None):

        if kernel is None:
            kernel = "rbf"

        if gamma is None:
            gamma = "auto"

        if C is None:
            C = 1.0

        if scale is None:
            scale = False

        self._svr = SVR(kernel=kernel, C=C, gamma=gamma)

        self._scale = scale

    def fit(self, x, y, ddof=None):

        if ddof is None:
            ddof = 0

        x2 = x if len(x.shape) == 2 else x.reshape(-1, 1)

        y2 = y

        if self._scale:

            x2, self._x_mean, self._x_std = standard_scale(x2, ddof)

            y2, self._y_mean, self._y_std = standard_scale(y2, ddof)

        self._svr.fit(x2, y2)

        return self

    def predict(self, x):
        x2 = x if len(x.shape) == 2 else x.reshape(-1, 1)

        if self._scale:

            x2 = scale(x2, self._x_mean, self._x_std)

        y2 = self._svr.predict(x2)

        return y2 if not self._scale\
            else scale_back(y2, self._y_mean, self._y_std)


def train_and_predict(x,
                      y,
                      x_predict,
                      svr_kwargs=None,
                      ddof=None):

    if svr_kwargs is None:
        svr_kwargs = {}

    return Svr(**svr_kwargs).fit(x, y, ddof).predict(x_predict)


def train_and_predict_many_impl(
        first,
        last,
        ids,
        data,
        prediction_id_column,
        date_column,
        value_column,
        prediction_start_date,
        prediction_end_date,
        svr_kwargs,
        ddof,
        output_column,
        use_logarithm):

    data = data[data[prediction_id_column].isin(ids[first:last])]\
        .reset_index(drop=True)

    # Make sure input is properly sorted.
    data = data.sort_values(by=[prediction_id_column,
                                date_column]).reset_index(drop=True)

    if use_logarithm:
        data[value_column] = data[value_column].replace(0, 1)
        data[value_column] = np.log(data[value_column])

    prediction_dates = pd.date_range(prediction_start_date,
                                     prediction_end_date,
                                     freq="MS")

    # Encoded (in days since epoch) dates used for prediction.
    encoded_prediction_dates = encode_dates(prediction_dates)

    # Number of values for which we do training and prediction.
    n = data[prediction_id_column].unique().size

    # Array where we store the predicted values
    # (let np.nan be the default - in # case something goes wrong).
    predicted = np.zeros((n, prediction_dates.size)) * np.nan

    count = 0

    for _, g in data.groupby(prediction_id_column, observed=True):

        try:

            logger.info((("train_and_predict_many_impl: Processing"
                          " dataset {}")).format(count))

            if g[prediction_id_column].shape[0] >= 12:

                encoded_dates = encode_dates(g[date_column])

                predicted[count] = train_and_predict(encoded_dates,
                                                     g[value_column].array,
                                                     encoded_prediction_dates,
                                                     svr_kwargs,
                                                     ddof)

        except Exception as e:
            logger.info("Ate an exception for id: {}."
                        .format(g[prediction_id_column].values[0]))

        count += 1

    # Transform the output into a more convenient format.
    result = \
        pd.DataFrame(np.repeat(data[prediction_id_column].unique(),
                               prediction_dates.size),
                     columns=[prediction_id_column])

    prediction_dates = prediction_dates.astype(str).str[:7]\
        .str.replace("-", "").astype(int)

    result[date_column] = np.tile(prediction_dates, n)

    if use_logarithm:
        predicted = np.exp(predicted)

    result[output_column] = predicted.reshape(-1)

    return result


def train_and_predict_many(data,
                           prediction_id_column,
                           date_column,
                           value_column,
                           prediction_start_date,
                           prediction_end_date,
                           svr_kwargs=None,
                           ddof=None,
                           output_column=None,
                           use_logarithm=None,
                           processes=None):

    if output_column is None:
        output_column = "predicted"

    if use_logarithm is None:
        use_logarithm = False

    ids = np.sort(data[prediction_id_column].unique())

    args = (ids,
            data,
            prediction_id_column,
            date_column,
            value_column,
            prediction_start_date,
            prediction_end_date,
            svr_kwargs,
            ddof,
            output_column,
            use_logarithm)

    result = iterate(train_and_predict_many_impl,
                     ids.size,
                     processes=processes,
                     args=args)

    return pd.concat(result,
                     axis=0,
                     ignore_index=True)
