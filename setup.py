#!/usr/bin/env python
#
# Last Modification Date: 2021-01-09
# Author: Gilberto Noronha <gnoronha@burning-glass.com>
#
# Please see the "README" file for basic installation and usage
# instructions.
#

import glob
import os
from setuptools import setup

package_name = "skills_projections"

install_requires = ["snowflake-helpers[full]==0.5",
                    "scikit-learn==0.24.0"]

current_directory, _ = os.path.split(__file__)
sql_scripts =\
    glob.glob(os.path.join(os.path.join(current_directory,
                                        package_name),
                           "sql") + "*.sql")

setup(name="skills-projections",
      version="0.1",
      packages=[package_name],
      package_data={package_name: sql_scripts},
      entry_points={"console_scripts": [(("skills_projections"
                                          " = skills_projections.command_line:main"))]},
      description="Tools for predicting future growth of skills.",
      author="Gilberto Noronha",
      author_email="gnoronha@burning-glass.com",
      install_requires=install_requires)
